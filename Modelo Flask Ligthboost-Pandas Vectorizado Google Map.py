





#tabla donde se configura la información que será presentada en cada uno de los puntos que detecte el sistema
table = """
<!DOCTYPE html>
<html>
<head>
<style>
table {{
    width:100%;
}}
table, th, td {{
    border: 1px solid black;
    border-collapse: collapse;
}}
th, td {{
    padding: 5px;
    text-align: left;
}}
table#t01 tr:nth-child(odd) {{
    background-color: #eee;
}}
table#t01 tr:nth-child(even) {{
   background-color:#fff;
}}
</style>
</head>
<body>
 
<table id="t01">
  <tr>
    <td>Area</td>
    <td>{}</td>
  </tr>
  <tr>
    <td>Estrato</td>
    <td>{}</td>
  </tr>
  <tr>
    <td>Alcoba</td>
    <td>{}</td>
  </tr>
  <tr>
    <td>Banos</td>
    <td>{}</td>
  </tr>
  <tr>
    <td>Parquerero</td>
    <td>{}</td>
  </tr>
  <tr>
    <td>Valor</td>
    <td>${}</td>
  </tr>
</table>
</body>
</html>
""".format





import pandas as pd
df=pd.read_csv('VENTA.txt',sep=';')
df.columns=['DEPARTAMENTO','CIUDAD','SUBZONA','BARRIO','TIPO_INMUEBLE','X','Y','VAL_VENTA','AREA_M2','ALCOBA','BANOS','GARAJES','ESTRATO']





diccio_tipo=dict()
diccio_departamento=dict()
diccio_tipo={'APARTAESTUDIO': 0,
 'APARTAMENTO': 1,
 'BODEGA': 2,
 'CABANA': 3,
 'CASA': 4,
 'CASA/APARTAMENTO':5,
 'COMERCIO': 6,
 'CONSULTORIO': 7,
 'EDIFICIO': 8,
 'FINCA': 9,
 'HOTEL': 10,
 'ISLA': 11,
 'JARDIN INFANTIL': 12,
 'LOCAL': 13,
 'LOTE': 14,
 'OFICINA': 15,
 'PARQUEADERO': 16,
 'SERVICIOS': 17}
diccio_departamento={'ANTIOQUIA':0,'CUNDINAMARCA':1,'VALLE DEL CAUCA':2}





#Versión conecta con posgresql
# -*- coding: utf-8 -*-
#Templates

from sklearn.metrics import r2_score
from sklearn.preprocessing import LabelEncoder
import math
import lightgbm as lgb
import requests
import locale
from joblib import load
from flask import Flask
from flask import render_template
from flask import send_file
from flask import request
from flask import redirect
from flask import session
from flask_wtf import CsrfProtect
from flask import Markup
from flask_apscheduler import APScheduler
import pandas as pd
import folium
import geocoder
import pyproj #Para coordenadas de colombia
import numpy as np
from wtforms import Form
from wtforms import StringField, TextField, IntegerField, FloatField, SelectField
from wtforms.fields.html5 import EmailField
from wtforms import validators
from sklearn.metrics import mean_squared_error#Permite determinar el error cuadrático medio
from datetime import datetime 
import psycopg2
import os
date=datetime.now() #Trae la fecha
locale.setlocale(locale.LC_ALL, '')#Fija el formato basado en la configuración regional del equipo
class CommentForm(Form):
    direccion=StringField('direccion',[validators.length(min=10,max=128,message='favor ingrese una dirección valida'),
                             validators.Required(message='La dirección es requerida')])
    departamento=SelectField('departamento', choices=[("ANTIOQUIA","ANTIOQUIA"),("CUNDINAMARCA","CUNDINAMARCA"),("VALLE DEL CAUCA","VALLE DEL CAUCA")])
    area=FloatField('area',[validators.NumberRange(min=1,max=5000,message='El área no puede ser mayor de 5000'),
                             validators.Required(message='El área debe escribirla con punto')])
    habitaciones=IntegerField('habitaciones',validators=[
                validators.Required(message='El campo habitaciones debe contener numeros enteros'),
                validators.NumberRange(min=0, max=1000,message='El número de habitaciones no puede ser mayor de 1000')])
    banos=IntegerField('banos',validators=[
                validators.Required(message='El campo baños debe contener numeros enteros'),
                validators.NumberRange(min=0, max=1000)])
    parqueaderos=IntegerField('parqueaderos',validators=[
                validators.Required(message='El campo parqueaderos debe contener numeros enteros'),
                validators.NumberRange(min=0, max=1000,message='El número de parqueaderos no debe ser mayor de 10000')])
    estrato=IntegerField('estrato',validators=[
                validators.Required(message='El campo estrato debe contener numeros enteros'),
                validators.NumberRange(min=0, max=10,message='El estrato no puede ser mayor de 10')])
    area_influencia=IntegerField('Area_influencia',validators=[
                validators.Required(message='El campo área de influencia debe contener numeros enteros'),
                validators.NumberRange(min=0, max=1000,message='El área de influencia no puede ser menor que 0 o mayor de 1000')])
    tipo=SelectField('classday', choices=[("APARTAMENTO","APARTAMENTO"),("CASA","CASA")])
class archivo:
    def __init__(self):
        dt = datetime.now()
        fecha=dt.year+dt.month+dt.day+dt.hour+dt.minute+dt.second+dt.microsecond
        self.nombre="testigo"+str(fecha)+".xlsx"
        self.nombremapa="mapa"+str(fecha)+".html"
        
class transformacion():
    def _init_(self):
        pass
    def tipo_inmueble(self,diccio_tipo,tipo):
        return ([x[1] for x in diccio_tipo.items() if x[0]==tipo][0])
    def departamento(self,diccio_departamento,departamento):
        return ([x[1] for x in diccio_departamento.items() if x[0]==departamento][0])

app=Flask(__name__)
sheduler=APScheduler()
app.secret_key='my_secret_key'
csrf=CsrfProtect(app)
@app.route('/',methods=['GET','POST'])
def index(): #Función donde se capturan los datos que vienen del formulario
    comment_form=CommentForm(request.form)
    if request.method=='POST' and comment_form.validate():
        session['direccion']=comment_form.direccion.data
        session['departamento']=comment_form.departamento.data
        session['area']=comment_form.area.data
        session['habitaciones']=comment_form.habitaciones.data
        session['banos']=comment_form.banos.data
        session['parqueaderos']=comment_form.parqueaderos.data
        session['estrato']=comment_form.estrato.data
        session['area_influencia']=comment_form.area_influencia.data
        session['tipo']=comment_form.tipo.data
        return redirect('/ejecucion')  
    title="Curso Flask"
    direccion=comment_form.direccion.data
    return render_template('indexxg.html',title=title,form=comment_form,latitude=direccion)
@app.route('/ejecucion',methods=["get", "post"])
def ejecucion():
    direccion=session['direccion'] + " , " +  session['departamento']
    print("Estoy en la segunda",direccion)
    departamento='ANTIOQUIA'
    #departamento=int(departamento)
    area=session['area']
    area=float(area)
    habitaciones=session['habitaciones']
    habitaciones=int(habitaciones)
    banos=session['banos']
    banos=int(banos)
    parqueaderos=session['parqueaderos']
    parqueaderos=int(parqueaderos)
    estrato=session['estrato']
    estrato=int(estrato)
    area_influencia=session['area_influencia']
    area_influencia=int(area_influencia)
    #print(area_influencia)
    tipo=session['tipo']
    #g=geocoder.osm(direccion)
    #try:
        #lat=g.latlng[0]
        #lon=g.latlng[1]
    #except:
        #lat='0'
        #lon='0'
    lon=-75.5953803590625
    lat=6.241082326773976
    if lat!='0' and lon!='0':
        p = pyproj.Proj(init = "epsg:3116")
        Xeini= p(lon, lat)[0]
        Ynini=p(lon, lat)[1]
        def distan(Xei,Yni):
            Xe= p(Xei, Yni)[0]
            Yn=p(Xei, Yni)[1]
            return round(math.sqrt((Xeini-Xe)**2+(Ynini-Yn)**2))
        #Vectorización
        func1= np.vectorize(distan)
        val=func1(df.X.values,df.Y.values)
        dfm=pd.DataFrame(val)
        dfm.columns=['Distancia'] 
        dfn=pd.concat([df,dfm],axis=1)#Se crea el nuevo dataframe agreagando la distancia del punto buscado a todos los avaluos de la base de datos
        print(dfn)
        data=dfn[(dfn['AREA_M2']>=0.8*area)&(dfn['AREA_M2']<=1.2*area)&(dfn['Distancia']<=area_influencia)&(dfn['BANOS']==banos)&(dfn['GARAJES']==parqueaderos)&(dfn['ALCOBA']==habitaciones)&(dfn['ESTRATO']==estrato)&(dfn['TIPO_INMUEBLE']==tipo)] #Permite capturar los datos dependiendo de la distancia que se defina
        print(data)
        def creacion_testigos(data1):#función donde se crea el archivo para los testigos y la información del mapa
            testigos=data1
            testigos=testigos.dropna()
            testigos=testigos.to_dict('records')# pasa el dataframe a lista
            return testigos
        data['VAL_VENTA']=data['VAL_VENTA'].astype('float')
        data1=data.dropna()
        print("Data 1", len(data1))
        no=archivo()
        nombre_archivo=no.nombre
        session['nombre_archivo']=nombre_archivo
        directory = 'temporales'
        file = nombre_archivo
        #Verifica si el directorio existe
        if not os.path.exists(directory):
            os.makedirs(directory)
        #Guarda el archivo de excel en el directorio temporales el cual se borrara todos los día a las 2:00 Am
        data.to_excel(os.path.join(directory, file))
        #mapa
        some_map=folium.Map(location=[lat, lon],zoom_start=60)
        folium.Circle(radius=area_influencia,location=[lat, lon],color='green').add_to(some_map)
        folium.CircleMarker(location = [lat, lon], radius = area_influencia, color = 'blue',fill_color = 'yellow').add_to(some_map)
        for row in data.itertuples():
            tabla=table(row.AREA_M2,row.ESTRATO,row.ALCOBA,row.BANOS,row.GARAJES,locale.format('%.2f', row.VAL_VENTA, grouping=True, monetary=True))
            some_map.add_child(folium.Marker(location=[row.Y,row.X],popup=tabla))
        some_map.add_child(folium.Marker(location=[lat, lon],tooltip='Referencia',icon=folium.Icon(color="black")))
        nombremapathml=no.nombremapa
        session['nombre_archivohtml']=nombremapathml
        some_map.save('temporales/'+nombremapathml)
        def estadisticas_testigos(data1):
            estadisticas=data1
            estadisticas['Valor_m2']=estadisticas['VAL_VENTA']/estadisticas['AREA_M2']
            promedio=estadisticas['Valor_m2'].mean()
            desviacion=estadisticas['Valor_m2'].std()
            coeficiente_variacion=desviacion/promedio*100
            kurtosis=estadisticas['Valor_m2'].kurtosis()
            asimetria=estadisticas['Valor_m2'].skew()
            promedio=locale.format('%.2f', promedio, grouping=True, monetary=True)#Cambia el formato del número al formato del equipo
            return(promedio,desviacion,coeficiente_variacion,kurtosis,asimetria)
        data['VAL_VENTA'] = np.log1p(data['VAL_VENTA'])#Transformación de la variable Val Venta
        data['TIPO_INMUEBLE']=data['TIPO_INMUEBLE'].str.strip()#Quita los espacios en blanco en los tipos de Inmueble
        label=LabelEncoder() #Codifica las variables para pasarlas de Texto a numéricas
        data['TIPO_INMUEBLE_CODIFICADO']=label.fit_transform(data['TIPO_INMUEBLE'])
        data['DEPARTAMENTO_CODIFICADO']=label.fit_transform(data['DEPARTAMENTO'])
        train=data
        if len(train)>=10:
            y=train['VAL_VENTA'] #Se le asigna a la variable y el precio que es lo que deseamos predecir
            X=train.drop(['VAL_VENTA','X','Y','Distancia'],axis=1).select_dtypes(exclude='object') #Elimina los campos y excluye los que no son numéricos 
            X=X.values #Se arman las variables independientes
            # Se construyen los datos de entreamiento y testeo se deja el 80% para entrenamiento y 20% para testeo
            from sklearn.model_selection import train_test_split
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=7)
            lgb_model = lgb.LGBMRegressor(learning_rate = 0.01,max_depth = 30, n_estimators = 1000)
            lgb_model.fit(X_train,y_train)
            tipo_inmueble_codificado=transformacion().tipo_inmueble(diccio_tipo, tipo)
            departamento_codificado=transformacion().departamento(diccio_departamento,departamento)                          
            #avaluo=pd.DataFrame({'AREA_M2':[area],'ALCOBA':[habitaciones],'BANO':[banos],'GARAJE':[parqueaderos],'ESTRATO':[estrato],'TIPO_INMUEBLE_CODIFICADO':[tipo_inmueble_codificado],'DEPARTAMENTO_CODIFICADO':[departamento_codificado]})
            #avaluo=avaluo.values
            avaluo=[[area,habitaciones,banos,parqueaderos,estrato,tipo_inmueble_codificado,departamento_codificado]]
            pred=lgb_model.predict(X_test)
            print(r2_score(pred,y_test))
            prediccion=np.expm1(lgb_model.predict(avaluo))[0]
            return render_template('userxg.html',prediccion=prediccion,promedio=estadisticas_testigos(data1)[0],desviacion=estadisticas_testigos(data1)[1],coeficiente=estadisticas_testigos(data1)[2],kurtosis=estadisticas_testigos(data1)[3],asimetria=estadisticas_testigos(data1)[4],lista=creacion_testigos(data1))
        else:
            return render_template('mensaje.html',informacion='La consulta no arrojo datos para predecir',lista=creacion_testigos(data))
    else:
        return render_template('sindireccion.html',informacion='La dirección ingresada no es correcta favor revisar')

def schedulerTask(): #Tarea programada que borra los archivos creados en la carpeta temporales
    files = os.listdir("temporales") # La ruta la copia tal cual del explorador y añade "\" entre carpetas. el ultimo queda libre sin "\\"
    for name in files:
        os.remove("temporales/%s"%name) # Igual se copia la ruta y se añade "\\%s" al final.
        print("Archivo",name,"eliminado con éxito")
@app.route('/download')
def download_file():
    nombre_archivo=session['nombre_archivo']
    p=os.path.join('temporales', nombre_archivo)
    return send_file(p,as_attachment=True)
@app.route('/map')
def map():
    nombre_mapa=session['nombre_archivohtml']
    p=os.path.join('temporales', nombre_mapa)
    return send_file(p)
if __name__ == '__main__':
    sheduler.add_job(id='archivo',func=schedulerTask,trigger='cron', hour=14,minute=21) #tarea que se activad a las 2:30 Am para borrar todos los archivos de la carpeta temporales
    sheduler.start()
    app.run()







